﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    class AnimatedSprite : DrawableGameComponent
    {
        // enum to handle how we play through animated frames
        public enum PlayBehaviour
        {
            NULL, // has not been defined
            LOOP, // once it reaches the end, goes to beginning
            BOUNCE, // once it reaches the end, it plays backwards toward the beginning
            ONCE // once it reaches the end, stops
        }

        // Member Variables
        private Texture2D m_texture = null;
        private Rectangle m_sourceRectangle = new Rectangle();
        private Rectangle m_destinationRectangle = new Rectangle();
        private Color m_color;
        private Game1 m_game;

        public Rectangle DestinationRectangle
        {
            get { return m_destinationRectangle; }
        }

        // Animation Variables
		int m_startFrame = 0;
		int m_endFrame = 0;
		int m_currentFrame = 0;
		int m_numberOfFrames = 0;
        PlayBehaviour m_playBehaviour = PlayBehaviour.NULL;
		int m_waitTime = 0; // number of millis to wait to show new frame
		int m_sequenceTimer = 0; // how many millis left
		bool m_inReverse = false; // determines if we do the PlayBehaviour in reverse (from m_endFrame to m_startFrame)


        public AnimatedSprite(Game1 game, String textureString, Rectangle sourceRectangle, 
            Rectangle destinationRectangle, Color color) :
            base(game)
        {
            m_game = game;
            m_texture = base.Game.Content.Load<Texture2D>(textureString);
            m_sourceRectangle = sourceRectangle;
            m_destinationRectangle = destinationRectangle;

            if (color == null)
                color = Color.White;
            else
                m_color = color;

            game.Components.Add(this);
        }

        public override void Initialize()
        {
            //base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            // go to next frame
            NextFrame(gameTime);
        }


        public override void Draw(GameTime gameTime)
        {
            m_game.MySpriteBatch.Draw(m_texture, m_destinationRectangle, m_sourceRectangle, m_color);
        }

        public void SetSequence(int startFrame, int endFrame, int currentFrame,
			int waitTime, PlayBehaviour pb, bool inReverse) 
        {
            m_startFrame = startFrame;
            m_endFrame = endFrame;
            m_currentFrame = currentFrame;
            m_waitTime = waitTime;
            m_playBehaviour = pb;
            m_inReverse = inReverse;
        }

        public void NextFrame(GameTime gameTime)
        {
            m_sequenceTimer -= gameTime.ElapsedGameTime.Milliseconds;
	        if(m_sequenceTimer > 0)
	        {
		        return;
	        }

	        // we only get here if m_SequenceTimer was 0
	        m_sequenceTimer = m_waitTime;

	        switch(m_playBehaviour)
	        {
	        case PlayBehaviour.LOOP:
		        {
			        if(m_inReverse)
			        {
				        --m_currentFrame;
				        if(m_currentFrame < m_startFrame || m_currentFrame > m_endFrame) // worried about wrap-around
				        {
					        m_currentFrame = m_endFrame;
				        }
			        }
			        else
			        {
				        if(++m_currentFrame > m_endFrame)
				        {
					        m_currentFrame = m_startFrame;
				        }
			        }
			        RefreshFrame();
		        }
		        break;
	        case PlayBehaviour.BOUNCE:
		        {
			        if(m_inReverse)
			        {
				        if(--m_currentFrame <= m_startFrame)
				        {
					        m_inReverse = false;
					        if(m_currentFrame < m_startFrame) m_currentFrame = m_startFrame;
				        }
			        }
			        else
			        {
				        if(++m_currentFrame >= m_endFrame)
				        {
					        m_inReverse = true;
					        if(m_currentFrame > m_endFrame) m_currentFrame = m_endFrame;
				        }
			        }
			        RefreshFrame();
		        }
		        break;
	        case PlayBehaviour.ONCE:
		        {
			        if(m_inReverse)
			        {
				        if(m_currentFrame > m_startFrame)
				        {
					        --m_currentFrame;
					        RefreshFrame();
				        }
			        }
			        else
			        {
				        if(m_currentFrame < m_endFrame)
				        {
					        ++m_currentFrame;
					        RefreshFrame();
				        }
			        }
		        }
		        break;
	        default:
		        {
		        }
                break;
	        }
        }

        /// <summary>
        /// Adjusts the m_sourceRectangle according to the value of m_currentFrame
        /// </summary>
        public void RefreshFrame()
        {
            // how many frames across is the texture?
            int frameWidth = m_texture.Width / m_sourceRectangle.Width;
            int frameHeight = m_texture.Height / m_sourceRectangle.Height; 

            // if we imagine that the texture is a 2D array... the xPos is the x index in that array for the frame
            int frameXPos = (int)m_currentFrame % frameWidth;
            int frameYPos = (int)m_currentFrame / frameWidth;

            m_sourceRectangle.X = m_sourceRectangle.Width * frameXPos;
            m_sourceRectangle.Y = m_sourceRectangle.Height * frameYPos;
        }

        public void Move(int x, int y)
        {
            m_destinationRectangle.X += x;
            m_destinationRectangle.Y += y;
        }



    }


}
