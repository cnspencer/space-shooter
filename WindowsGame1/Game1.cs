using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        PlayerObject playerObject;
        Background background;
        private CollisionManager collisionManager;

        // Properties
        public SpriteBatch MySpriteBatch
        {
            get
            {
                return spriteBatch;
            }
            set
            {
                spriteBatch = value;
            }
        }
        public CollisionManager CollisionManager { get { return collisionManager; } }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            collisionManager = new CollisionManager(this);

            //Microsoft.Xna.Framework.Audio.
            
            //this.graphics.PreferredBackBufferWidth = 1366;
            //this.graphics.PreferredBackBufferHeight = 768;
            //this.graphics.IsFullScreen = true;

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {

            base.IsMouseVisible = true;

            base.Initialize();
            background = new Background(this, "Stars", Color.White);
            background.Initialize();

            Rectangle screenRect = base.Window.ClientBounds;

            playerObject = new PlayerObject(this, 
                "PlayerShip",
                new Rectangle(0, 0, 32, 32),
                new Rectangle(0, screenRect.Bottom - 128, 32, 32), Color.White);
            playerObject.Initialize();

            EnemyOjbect enemy = new EnemyOjbect(this, playerObject, "Enemy",
                new Rectangle(0, 0, 32, 32),
                new Rectangle(350, 0, 32, 32), Color.White);
            enemy.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
//            String s = Content.RootDirectory; 
//            Content.RootDirectory = "C:/Users/Charles/Documents/Visual Studio 2008/Projects/WindowsGame1/WindowsGame1/Content";
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            spriteBatch.Dispose();
            this.Content.Unload();

        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            #if XBOX
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            #else
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
            #endif

            collisionManager.CheckCollisions(gameTime);

            base.Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Magenta);

            // TODO: Add your drawing code here
            spriteBatch.Begin(SpriteBlendMode.AlphaBlend,
                SpriteSortMode.Immediate, SaveStateMode.None);

            
            //GraphicsDevice.RenderState.AlphaBlendEnable = true;
            //GraphicsDevice.RenderState.SourceBlend = Blend.One;
            //GraphicsDevice.RenderState.DestinationBlend = Blend.One;


            GraphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
            GraphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
           // GraphicsDevice.

            base.Draw(gameTime);


            spriteBatch.End();
        }
    }
}
