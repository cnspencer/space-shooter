﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    class Background : DrawableGameComponent
    {
        private Texture2D texture = null;
        private Rectangle sourceRectangle = new Rectangle();
        private Rectangle destinationRectangle = new Rectangle();
        private Color color = Color.White;

        private int backgroundDirection = 0; // 0 is straight, -1 is left, +1 is right

        public Background(Game1 game, String textureString, Color color) :
            base(game)
        {
            this.texture = base.Game.Content.Load <Texture2D>(textureString);
            Rectangle screenRect = base.Game.Window.ClientBounds;
            this.destinationRectangle = screenRect;
            this.destinationRectangle.X = 0;
            this.destinationRectangle.Y = 0;
            this.sourceRectangle = new Rectangle();
            this.sourceRectangle.X = 0;
            this.sourceRectangle.Y = 0;
            this.sourceRectangle.Width = screenRect.Width;
            this.sourceRectangle.Height = screenRect.Height;
            this.color = color;

            game.Components.Add(this);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            this.sourceRectangle.X += backgroundDirection;
            this.sourceRectangle.Y -= 2;
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.RenderState.AlphaBlendEnable = false;
            GraphicsDevice.RenderState.SourceBlend = Blend.One;
            GraphicsDevice.RenderState.DestinationBlend = Blend.One;

            ((Game1)(base.Game)).MySpriteBatch.Draw(texture, destinationRectangle, sourceRectangle, color);


            GraphicsDevice.RenderState.AlphaBlendEnable = false;
        }
    }
}
