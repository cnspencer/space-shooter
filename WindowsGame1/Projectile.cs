﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    //abstract 
    class Projectile : GameComponent, Collidable
    {

        public enum DamageType
        {
            NULL,
            ENERGY,
            FIRE,
            COLD,
            DARK
        }

        protected AnimatedSprite m_animatedSprite = null;
        protected int m_damage = 1;
        protected int m_speed = 1; // pixels per tick
        Game1 m_game = null;
        protected DamageType m_damageType = DamageType.ENERGY;

        private CollisionPoint m_collisionPoint = new CollisionPoint();
        public CollisionPoint CollisionPoint
        {
            get
            {
                return m_collisionPoint;
            }
        }
        public CollisionArea CollisionArea { get { return m_collisionPoint; } }

        public bool IsGoingForward
        {
            get
            {
                return m_speed >= 0;
            }
            set
            {
                if (value)
                    m_speed = Math.Abs(m_speed);
                else
                    m_speed = -Math.Abs(m_speed);
            }
        }

        public Projectile(Game1 game, int damage, int speed, String textureString, Rectangle sourceRectangle, 
            Rectangle destinationRectangle, Color color, DamageType damageType) :
            base(game)
        {
            m_damageType = damageType;
            m_damage = damage;
            m_speed = speed;
            m_game = game;
            game.Components.Add(this);

            m_animatedSprite = new AnimatedSprite(game, textureString, sourceRectangle, destinationRectangle, color);
            m_animatedSprite.SetSequence(0, 3, 0, 250, AnimatedSprite.PlayBehaviour.LOOP, false);
        }

        public override void Initialize()
        {
            base.Initialize();
            m_game.CollisionManager.CollidableList.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            // check if we went off screen, in which case we will get rid of ourselves
            Rectangle screenRect = m_game.Window.ClientBounds;
            Rectangle spriteRect = m_animatedSprite.DestinationRectangle;
            if (spriteRect.Bottom < 0 || spriteRect.Top > screenRect.Bottom)
            {
                m_game.CollisionManager.CollidableList.Remove(this);
                m_game.Components.Remove(m_animatedSprite);
                m_game.Components.Remove(this);

            }

            //m_animatedSprite.Update(gameTime);
            m_animatedSprite.Move(0, m_speed);//* gameTime.ElapsedGameTime.Milliseconds);

            Rectangle animRect = m_animatedSprite.DestinationRectangle;
            m_collisionPoint.X = animRect.X + (animRect.Width / 2);
            m_collisionPoint.Y = animRect.Y + (animRect.Height / 2);

            base.Update(gameTime);
        }

        //------ from Collidable ------
        public void OnCollision(Collidable collidable)
        {
        }
    }
}
