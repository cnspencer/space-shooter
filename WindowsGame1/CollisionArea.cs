﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    //-----------------------------------------
    //------ Interfaces -----------------------
    //-----------------------------------------
    interface CollisionArea
    {
        bool Collides(CollisionArea collisionArea);
    }

    interface Collidable
    {
        CollisionArea CollisionArea { get; }

        void OnCollision(Collidable collidable);
    }

    //-----------------------------------------
    //------ Manager --------------------------
    //-----------------------------------------
    public class CollisionManager
    {
        private Collection<Collidable> collidableList;
        internal Collection<Collidable> CollidableList { get { return collidableList; } }
        Game1 game = null;

        //------ Lifetime ------
        public CollisionManager(Game1 game)
        {
            collidableList = new Collection<Collidable>();
            this.game = game;
        }

        // check all collisiones and call OnCollision on any objects that collide
        public void CheckCollisions(GameTime gameTime)
        {
            int length = collidableList.Count;

            for (int i = 0; i < length; ++i)
            {
                for (int j = i + 1; j < length; ++j)
                {
                    Collidable first = collidableList.ElementAt(i);
                    Collidable second = collidableList.ElementAt(j);
                    if (first.CollisionArea.Collides(
                        second.CollisionArea))
                    {
                        first.OnCollision(second);
                    }
                }
            }
        }
    }

    //-----------------------------------------
    //------ Implementations ------------------
    //-----------------------------------------

    class CollisionPoint : CollisionArea
    {
        private Point point;
        public Point Point
        {
            get{ return point; }
            set{ point = value; }
        }
        public int X
        {
            get { return point.X; }
            set { point.X = value; }
        }
        public int Y
        {
            get { return point.Y; }
            set { point.Y = value; }
        }

        //------ Lifetime ------
        public CollisionPoint() { point = new Point(0, 0); }
        public CollisionPoint(Point point) { Point = point; }

        //------ From CollisionArea ------
        public bool Collides(CollisionArea collisionArea)
        {
            if (collisionArea is CollisionPoint)
            {
                CollisionPoint collisionPoint = (CollisionPoint)collisionArea;
                return this.Point.Equals(collisionPoint.Point);
            }
            else
            {
                return collisionArea.Collides(this);
            }
        }
    }

    // Note: this is an axis-aligned rectangle
    class CollisionRectangle : CollisionArea
    {
        private Rectangle rectangle;
        public Rectangle Rectangle
        {
            get { return rectangle; }
            set { rectangle = value; }
        }
        
        public int X
        {
            get { return rectangle.X; }
            set{rectangle.X = value;}
        }
        public int Y
        {
            get { return rectangle.Y; }
            set { rectangle.Y = value; }
        }
        public int Width
        {
            get { return rectangle.Width; }
            set { rectangle.Width = value; }
        }
        public int Height
        {
            get { return rectangle.Height; }
            set { rectangle.Height = value; }
        }

        //------ Lifetime ------
        public CollisionRectangle() { Rectangle = new Rectangle(); }
        public CollisionRectangle(Rectangle rectangle) { Rectangle = rectangle; }

        //------ From CollisionArea ------
        public bool Collides(CollisionArea collisionArea)
        {
            if (collisionArea is CollisionPoint)
            {
                CollisionPoint collisionPoint = (CollisionPoint)collisionArea;
                return collisionPoint.Point.X >= Rectangle.X && 
                    collisionPoint.Point.X <= Rectangle.X + Rectangle.Width &&
                    collisionPoint.Point.Y >= Rectangle.Y &&
                    collisionPoint.Point.Y <= Rectangle.Y + Rectangle.Height;
            }
            else if (collisionArea is CollisionRectangle)
            {
                CollisionRectangle collisionRectangle = (CollisionRectangle)collisionArea;
                return Rectangle.X <= collisionRectangle.Rectangle.X + collisionRectangle.Rectangle.Width &&
                    collisionRectangle.Rectangle.X <= Rectangle.X + Rectangle.Width &&
                    Rectangle.Y <= collisionRectangle.Rectangle.Y + collisionRectangle.Rectangle.Height &&
                    collisionRectangle.Rectangle.Y <= Rectangle.Y + Rectangle.Height;
            }
            else
            {
                return collisionArea.Collides(this);
            }
        }
    }
}
