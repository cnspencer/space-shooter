﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

// This file actuall contains 3 clases for a model-view-controller implementation of game objects.
//
// Model:      The GameObjectModel class.  Maintains state and collision detection info and tries to carry out
//             the wishes of it's associated GameObject.  Also updates the GameObjectView.
//
// View:       The GameObjectView class.  Maintains any AnimatedSprites or other visual/audio effects for the class.
//
// Controller: The GameObjectController class.  Override it to implement a player game object, or an AI game object, or a
//             simple game object like a projectile.

namespace WindowsGame1
{
    abstract class GameObjectModel : GameComponent
    {
        public GameObjectModel(Game1 game) :
            base(game)
        {
        }

        // properties
        public abstract CollisionArea CollisionArea { get; set; }

        // functions
        public abstract void Move(Vector2 displacement);
    }

    abstract class GameObjectView : GameComponent // not DrawableGameComponent, instead it will contain DrawableGameComponents
    {
        public GameObjectView(Game1 game) :
            base(game)
        {
        }

        // properties
        public abstract AnimatedSprite AnimatedSprite{ get; set; }
        
        // functions
        public abstract void Move(Vector2 displacement);
    }

    abstract class GameObjectController : GameComponent
    {
        public GameObjectController(Game1 game) :
            base(game)
        {
        }

        public abstract Vector2 PreferredVelocity { get; set; }
    }
}
