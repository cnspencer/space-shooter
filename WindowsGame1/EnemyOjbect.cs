﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    class EnemyOjbect : GameComponent, Collidable
    {
        private Game1 m_game = null;
        private AnimatedSprite m_animatedSprite = null;
        private CollisionRectangle m_collisionRectangle;
        public CollisionArea CollisionArea { get { return m_collisionRectangle; } }
        private bool m_isDead = false;
        public bool IsDead { get { return m_isDead; } }

        private PlayerObject m_player;

        private int m_projectileWaitTime = 250;
        private int m_projectileTimer = 0;


        public EnemyOjbect(Game1 game, PlayerObject player, String textureString, Rectangle sourceRectangle, 
            Rectangle destinationRectangle, Color color) :
            base(game)
        {
            m_game = game;
            m_player = player;
            m_collisionRectangle = new CollisionRectangle(new Rectangle());
            m_collisionRectangle.X = destinationRectangle.X + destinationRectangle.Width / 4;
            m_collisionRectangle.Y = destinationRectangle.Y + destinationRectangle.Height / 4;
            m_collisionRectangle.Width = destinationRectangle.Width / 2;
            m_collisionRectangle.Height = destinationRectangle.Height / 2;

          
            m_game.Components.Add(this);


            m_animatedSprite = new AnimatedSprite(game, textureString, sourceRectangle, destinationRectangle, color);
            m_animatedSprite.Initialize();
            //m_animatedSprite.SetSequence(0, 3, 0, 500, AnimatedSprite.PlayBehaviour.LOOP, false);
        }

        public override void Initialize()
        {
            base.Initialize();
            m_game.CollisionManager.CollidableList.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            if (m_isDead)
            {
                m_game.CollisionManager.CollidableList.Remove(this);
                return;
            }

            CollisionRectangle playerRect = m_player.CollisionRectangle;

            // fire if player in front
            m_projectileTimer -= gameTime.ElapsedGameTime.Milliseconds;
            if (m_projectileTimer < 0 && !m_player.IsDead)
            {
                if (playerRect.X < m_collisionRectangle.X + m_collisionRectangle.Width &&
                    playerRect.X + playerRect.Width > m_collisionRectangle.X)
                {
                    m_projectileTimer = m_projectileWaitTime;
                    FireProjectile();
                }
                else
                {
                    m_projectileTimer = -1;
                }
            }
            // move toward player (get within 34 pixels)
            int playerCenterX = playerRect.X + playerRect.Width / 2;
            int myCenterX = m_collisionRectangle.X + m_collisionRectangle.Width / 2;
            if (myCenterX - 3 > playerCenterX)
                Move(-2, 0);
            else if (myCenterX + 4 < playerCenterX)
                Move(2, 0);

        }


        public void Move(int x, int y)
        {
            m_animatedSprite.Move(x, y);
            m_collisionRectangle.X += x;
            m_collisionRectangle.Y += y;
        }

        public void FireProjectile()
        {
            Rectangle projSourceRect = new Rectangle(0, 0, 8, 8);
            Rectangle projDestRect = new Rectangle();

            projDestRect.X = m_collisionRectangle.X + ((m_collisionRectangle.Width - projSourceRect.Width) / 2);
            projDestRect.Y = (m_collisionRectangle.Y + m_collisionRectangle.Height) + projSourceRect.Height;
            projDestRect.Width = projSourceRect.Width;
            projDestRect.Height = projSourceRect.Height;

            Projectile projectile = new Projectile(m_game, 1, 3, "Projectile",
                projSourceRect, projDestRect, Color.White, Projectile.DamageType.ENERGY);
        }

        //------ From Collidable ------
        public void OnCollision(Collidable collidable)
        {
            m_animatedSprite = new AnimatedSprite(m_game, "Explosion", new Rectangle(0, 0, 32, 32),
                m_animatedSprite.DestinationRectangle, Color.White);
            m_animatedSprite.SetSequence(0, 3, 0, 50, AnimatedSprite.PlayBehaviour.LOOP, false);
            m_isDead = true;
        }
    }
}
